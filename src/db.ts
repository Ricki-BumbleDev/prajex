import { Db, MongoClient } from 'mongodb';

let db: Db;

export const connectToDb = async (dbUrl: string, dbName: string) =>
  (db = await MongoClient.connect(dbUrl).then(client => client.db(dbName)));

const getDb = () => db;

export const getAutoIncrementId = async (key: string) => {
  const document = await getDb()
    .collection<{ _id: string; seq: number }>('sequences')
    .findOneAndUpdate(
      { _id: `autoid_${key}` },
      { $inc: { seq: 1 }, $setOnInsert: { n: 1 } },
      {
        upsert: true,
        returnOriginal: false
      }
    );
  return document.value.seq;
};

export const tasksQuery = [
  {
    $lookup: {
      from: 'users',
      localField: 'assigneeId',
      foreignField: '_id',
      as: 'assignee'
    }
  },
  { $unwind: '$assignee' },
  {
    $lookup: {
      from: 'users',
      localField: 'createdById',
      foreignField: '_id',
      as: 'createdBy'
    }
  },
  { $unwind: '$createdBy' },
  {
    $project: {
      _id: true,
      title: true,
      status: true,
      description: true,
      assignee: { _id: true, firstName: true, lastName: true },
      createdAt: true,
      createdBy: { _id: true, firstName: true, lastName: true }
    }
  }
];

export const getTaskSearchQuery = (searchTerm: string) => [
  {
    $match: {
      $or: [
        {
          title: {
            $regex: new RegExp('.*' + searchTerm + '.*', 'i')
          }
        },
        {
          description: {
            $regex: new RegExp('.*' + searchTerm + '.*', 'i')
          }
        }
      ]
    }
  },
  ...tasksQuery
];

export default getDb;
