export interface DbTask {
  _id: number;
  title: string;
  status: Status;
  description: string;
  assigneeId: number;
  createdAt: Date;
  createdById: number;
}

export interface Task {
  _id: number;
  title: string;
  status: Status;
  description: string;
  assignee: User;
  createdAt: Date;
  createdBy: User;
}

export interface User {
  _id: number;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
}

export enum Status {
  BACKLOG = 'BACKLOG',
  TODO = 'TODO',
  INPROGRESS = 'INPROGRESS',
  DONE = 'DONE',
  ARCHIVE = 'ARCHIVE'
}
