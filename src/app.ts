import bcrypt from 'bcrypt';
import express from 'express';
import authorize from 'express-jwt';
import jwt from 'jsonwebtoken';
import path from 'path';
import { connectToDb, getAutoIncrementId, getTaskSearchQuery, tasksQuery } from './db';
import { Task, User } from './types';

const port = process.env.PORT || 5000;
const jwtSecret = process.env.JWT_SECRET || 'hecyL1GWBoZYH17FbclL6mDMX825znqI';
const dbUrl = process.env.DB_URL || 'mongodb://localhost:27017';
const dbName = process.env.DB_NAME || 'prajex';

connectToDb(dbUrl, dbName).then(db => {
  const app = express();

  app.use(
    ['/users', '/tasks'],
    authorize({ secret: jwtSecret, requestProperty: 'auth' }).unless({
      path: ['/users/login']
    }),
    express.json()
  );

  const getUser = (id: number) =>
    db.collection('users').findOne<User>({ _id: id }, { projection: { password: false } });

  const getTask = (id: number) =>
    db
      .collection('tasks')
      .aggregate<Task>([{ $match: { _id: id } }, { $limit: 1 }, ...tasksQuery])
      .next();

  app.get('/tasks/list', (req, res) =>
    db
      .collection('tasks')
      .aggregate<Task>(tasksQuery)
      .sort({ _id: 1 })
      .toArray()
      .then(tasks => res.json(tasks))
  );

  app.get('/tasks/get/:id', (req, res) => getTask(Number(req.params.id)).then(task => res.json(task)));

  app.post('/tasks/create', (req: express.Request & { auth: { user: User } }, res) =>
    getAutoIncrementId('tasks')
      .then(id =>
        db.collection('tasks').insert({
          _id: id,
          title: req.body.title,
          status: req.body.status,
          description: req.body.description,
          assigneeId: req.body.assigneeId,
          createdAt: new Date(),
          createdById: req.auth.user._id
        })
      )
      .then(result => res.json(result.ops[0]))
  );

  app.post('/tasks/update/:id', (req, res) =>
    db
      .collection('tasks')
      .update({ _id: Number(req.params.id) }, req.body)
      .then(data => res.json(data))
  );

  app.get('/tasks/delete/:id', (req, res) =>
    db
      .collection('tasks')
      .remove({ _id: Number(req.params.id) })
      .then(data => res.json(data))
  );

  app.post('/tasks/search', (req, res) =>
    db
      .collection('tasks')
      .aggregate(getTaskSearchQuery(req.body.searchTerm))
      .toArray()
      .then(task => res.json(task))
  );

  app.get('/users/list', (req, res) =>
    db
      .collection('users')
      .find({}, { projection: { password: false } })
      .sort({ lastName: 1 })
      .toArray()
      .then(users => res.json(users))
  );

  app.get('/users/get/me', (req: express.Request & { auth: { user: User } }, res) =>
    getUser(req.auth.user._id).then(user => res.json(user))
  );

  app.post('/users/register', (req, res) => res.status(404).json({ message: 'Users registration not yet available' }));

  app.post('/users/update/me', (req: express.Request & { auth: { user: User } }, res) =>
    db
      .collection('users')
      .update({ _id: req.auth.user._id }, req.body)
      .then(data => res.json(data))
  );

  app.get('/users/delete/me', (req: express.Request & { auth: { user: User } }, res) =>
    db
      .collection('users')
      .remove({ _id: req.params._id })
      .then(data => res.json(data))
  );

  app.post('/users/login', async (req, res) => {
    const user = await db.collection('users').findOne({ email: req.body.email });
    if (user && (await bcrypt.compare(req.body.password, user.password))) {
      const token = jwt.sign(
        {
          user: {
            _id: user._id,
            firstName: user.firstName,
            lastName: user.lastName
          }
        },
        jwtSecret,
        { expiresIn: 7 * 24 * 60 * 60 }
      );
      res.json({ token });
    } else {
      res.status(400).json({ message: 'Wrong username or password' });
    }
  });

  app.use(express.static(path.join(__dirname, 'static')));

  app.get('*', (req, res) => res.sendFile(path.join(__dirname, 'static', 'index.html')));

  app.listen(port, () => console.log('App listening on port ' + port));
});
