import * as React from 'react';
import { RouteProps } from 'react-router';
import { BrowserRouter, Redirect, Route, RouteComponentProps, Switch } from 'react-router-dom';
import BacklogPage from './pages/backlog';
import BoardPage from './pages/board';
import LoginPage from './pages/login';
import SearchPage from './pages/search';
import TaskPage from './pages/task';
import { isLoggedIn } from './services/auth';
import './style.css';

class App extends React.Component {
  public render = () => (
    <BrowserRouter>
      <Switch>
        <Route path="/login" component={LoginPage} />
        <ProtectedRoute exact path="/" component={() => <Redirect to="/board" />} />
        <ProtectedRoute path="/board" component={BoardPage} />
        <ProtectedRoute path="/backlog" component={BacklogPage} />
        <ProtectedRoute path="/task" component={TaskPage} />
        <ProtectedRoute path="/search" component={SearchPage} />
      </Switch>
    </BrowserRouter>
  );
}

export interface ProtectedRouteProps extends RouteProps {
  component: React.ComponentType<RouteComponentProps<any>> | React.ComponentType<any>;
}

const ProtectedRoute = ({ component: Component, ...rest }: ProtectedRouteProps) => (
  <Route {...rest} render={props => (isLoggedIn() ? <Component {...props} /> : <Redirect to="/login" />)} />
);

export default App;
