import * as React from 'react';

const LoadingIndicator = () => (
    <div className="lds-ellipsis lds">
      <div />
      <div />
      <div />
      <div />
    </div>
);

export default LoadingIndicator;
