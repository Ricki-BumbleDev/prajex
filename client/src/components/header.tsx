import React from 'react';
import { Link, RouteComponentProps, withRouter } from 'react-router-dom';
import { logout } from '../services/auth';

const Header = (props: RouteComponentProps) => (
  <div className="header">
    <Link to="/board" className="header-item">
      <img className="brand" src="/brand.svg" alt="prajex" />
    </Link>
    <Link to="/board" className="header-item nav-item">
      Board
    </Link>
    <Link to="/backlog" className="header-item nav-item">
      Backlog
    </Link>
    <Link to="/search" className="header-item nav-item">
      Search
    </Link>
    <Link to="/task/new" className="header-item nav-item">
      New task
    </Link>
    <div
      className="header-item nav-item nav-item-right"
      onClick={() => {
        logout();
        props.history.push('/login');
      }}
    >
      Logout
    </div>
  </div>
);

export default withRouter(Header);
