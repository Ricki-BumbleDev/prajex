import * as React from 'react';
import { Task } from '../types';
import { Link } from 'react-router-dom';
import { DraggableProvided } from 'react-beautiful-dnd';

const Card = (props: {
  task: Task;
  provided?: DraggableProvided;
  style?: React.CSSProperties;
}) => (
  <Link to={'/task/' + props.task._id}>
    <div
      className="card"
      ref={props.provided && props.provided.innerRef}
      {...props.provided && props.provided.draggableProps}
      {...props.provided && props.provided.dragHandleProps}
      style={props.style}
    >
      <div className="card-header">
        <div className="card-header-id">#{props.task._id}</div>
        <div className="card-header-title">{props.task.title}</div>
      </div>
      <div className="card-body">
        <div className="card-body-description">{props.task.description}</div>
        <div className="card-body-assignee">
          {props.task.assignee.firstName + ' ' + props.task.assignee.lastName}
        </div>
      </div>
    </div>
  </Link>
);

export default Card;
