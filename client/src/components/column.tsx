import * as React from 'react';
import { Task } from '../types';
import Card from './card';

const Column = (props: {
  title?: string;
  droppableId?: string;
  tasks: Task[];
}) => (
  <div className="column">
    {props.title && <div className="column-header heading">{props.title}</div>}
    <div className="cards">
      {props.tasks.map(task => (
        <Card task={task} key={task._id} />
      ))}
    </div>
  </div>
);

export default Column;
