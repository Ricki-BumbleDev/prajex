import * as React from 'react';
import { Task } from '../types';
import Card from './card';
import { Droppable, Draggable } from 'react-beautiful-dnd';

const DragAndDropColumn = (props: {
  title: string;
  droppableId: string;
  tasks: Task[];
}) => (
  <div className="column">
    <div className="column-header heading">{props.title}</div>
    <Droppable droppableId={props.droppableId}>
      {(provided, snapshot) => (
        <div className="cards" ref={provided.innerRef}>
          {props.tasks.map((task, index) => (
            <Draggable
              key={task._id}
              draggableId={task._id.toString()}
              index={index}
            >
              {(_provided, _snapshot) => (
                <Card
                  task={task}
                  provided={_provided}
                  style={{
                    ..._provided.draggableProps.style,
                    ...(_snapshot.isDragging && {
                      boxShadow: '0 0 20px lightgrey'
                    })
                  }}
                />
              )}
            </Draggable>
          ))}
          {provided.placeholder}
        </div>
      )}
    </Droppable>
  </div>
);

export default DragAndDropColumn;
