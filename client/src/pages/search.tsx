import React from 'react';
import { onChangeInput } from '../util';
import Column from '../components/column';
import Header from '../components/header';
import LoadingIndicator from '../components/loading-indicator';
import { configuredFetch } from '../services/auth';
import { Task } from '../types';

interface SearchPageState {
  searchTerm: string;
  loading: boolean;
  tasks: Task[];
}

export default class SearchPage extends React.Component<{}, SearchPageState> {
  public state: SearchPageState = {
    searchTerm: '',
    loading: false,
    tasks: []
  };

  private onChange = async (value: string) =>
    this.setState(
      {
        searchTerm: value,
        loading: true
      },
      () =>
        configuredFetch('/tasks/search', {
          method: 'POST',
          body: JSON.stringify({ searchTerm: value })
        }).then(tasks =>
          this.setState({
            tasks,
            loading: false
          })
        )
    );

  public render = () => (
    <div className="container">
      <Header />
      <div className="content-container">
        <input
          placeholder="Search..."
          onChange={onChangeInput(this.onChange)}
        />
      </div>
      {this.state.searchTerm !== '' &&
        (this.state.loading ? (
          <LoadingIndicator />
        ) : this.state.tasks.length > 0 ? (
          <div className="columns">
            <Column tasks={this.state.tasks} />
          </div>
        ) : (
          <div className="content-container">No matches</div>
        ))}
    </div>
  );
}
