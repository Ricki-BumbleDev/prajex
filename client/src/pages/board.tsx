import * as React from 'react';
import { DragDropContext } from 'react-beautiful-dnd';
import DragAndDropColumn from '../components/drag-and-drop-column';
import Header from '../components/header';
import LoadingIndicator from '../components/loading-indicator';
import { configuredFetch } from '../services/auth';
import { Status, Task, TaskInput } from '../types';
import { clone } from '../util';

interface BoardPageState {
  loading: boolean;
  tasks: Task[];
}

export default class BoardPage extends React.Component<{}, BoardPageState> {
  public state: BoardPageState = {
    loading: true,
    tasks: []
  };

  public componentDidMount = async () =>
    this.setState({
      tasks: await configuredFetch('/tasks/list'),
      loading: false
    });

  private getFormValue = (task: Task) => {
    const formValue: any = clone(task);
    formValue.assigneeId = task.assignee._id;
    formValue.assignee = undefined;
    formValue.createdById = task.createdBy._id;
    formValue.createdBy = undefined;
    return formValue as TaskInput;
  };

  private updateTask = (task: Task) => {
    configuredFetch('/tasks/update/' + task._id, {
      method: 'POST',
      body: JSON.stringify(this.getFormValue(task))
    });
  };

  public render = () => (
    <div className="container">
      <Header />
      {this.state.loading ? (
        <LoadingIndicator />
      ) : (
        <DragDropContext
          onDragEnd={event => {
            if (event.destination) {
              const tasks = clone(this.state.tasks);
              const index = tasks.findIndex(task => task._id === Number(event.draggableId));
              tasks[index].status = event.destination.droppableId as Status;
              this.setState({ tasks });
              this.updateTask(tasks[index]);
            }
          }}
        >
          <div className="columns">
            <DragAndDropColumn
              title="To do"
              droppableId={Status.TODO}
              tasks={this.state.tasks.filter(task => task.status === Status.TODO)}
            />
            <DragAndDropColumn
              title="In progress"
              droppableId={Status.INPROGRESS}
              tasks={this.state.tasks.filter(task => task.status === Status.INPROGRESS)}
            />
            <DragAndDropColumn
              title="Done"
              droppableId={Status.DONE}
              tasks={this.state.tasks.filter(task => task.status === Status.DONE)}
            />
          </div>
        </DragDropContext>
      )}
    </div>
  );
}
