import React from 'react';
import { onChangeInput } from '../util';
import { login } from '../services/auth';
import { RouteComponentProps } from 'react-router';

interface LoginPageState {
  email: string;
  password: string;
  errorMessage?: string;
}

export default class LoginPage extends React.Component<RouteComponentProps, LoginPageState> {
  public state: LoginPageState = {
    email: '',
    password: ''
  };

  private onChange = <K extends keyof LoginPageState>(key: K) => (value: LoginPageState[K]) => {
    this.setState(prevState => ({ ...prevState, [key]: value }));
  };

  private onSubmit = async () => {
    login(this.state.email, this.state.password)
      .then(() => this.props.history.push('/board'))
      .catch(err => this.setState({ errorMessage: err.message }));
  };

  public render = () => {
    return (
      <div className="login-background">
        <div className="login-container">
          <div className="login-container">
            <div className="login-header">
              <img className="login-brand" src="/brand.svg" alt="prajex" />
            </div>
            <form
              onSubmit={event => {
                event.preventDefault();
                this.onSubmit();
              }}
            >
              <div className="simple-form-row">
                <input placeholder="Email" value={this.state.email} onChange={onChangeInput(this.onChange('email'))} />
              </div>
              <div className="simple-form-row">
                <input
                  placeholder="Password"
                  type="password"
                  value={this.state.password}
                  onChange={onChangeInput(this.onChange('password'))}
                />
              </div>
              {this.state.errorMessage && <div className="simple-form-row">{this.state.errorMessage}</div>}
              <div className="simple-form-row">
                <button>Login</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  };
}
