import React from 'react';
import Header from '../components/header';
import { Task, Status } from '../types';
import Column from '../components/column';
import LoadingIndicator from '../components/loading-indicator';
import { configuredFetch } from '../services/auth';

interface BacklogPageState {
  loading: boolean;
  tasks: Task[];
}

export default class BacklogPage extends React.Component<{}, BacklogPageState> {
  public state: BacklogPageState = {
    loading: true,
    tasks: []
  };

  public componentDidMount = async () =>
    this.setState({
      tasks: await configuredFetch('/tasks/list'),
      loading: false
    });

  public render = () => (
    <div className="container">
      <Header />
      {this.state.loading ? (
        <LoadingIndicator />
      ) : (
        <div className="columns">
          <Column
            title="Backlog"
            tasks={this.state.tasks.filter(task => task.status === Status.BACKLOG)}
          />
        </div>
      )}
    </div>
  );
}
