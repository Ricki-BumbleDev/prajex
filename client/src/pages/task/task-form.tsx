import moment from 'moment';
import React from 'react';
import LoadingIndicator from '../../components/loading-indicator';
import { configuredFetch } from '../../services/auth';
import { Status, Task, TaskInput, User } from '../../types';
import { clone, onChangeInput } from '../../util';

const statusOptions = [
  {
    label: 'Backlog',
    value: Status.BACKLOG
  },
  {
    label: 'To do',
    value: Status.TODO
  },
  {
    label: 'In progress',
    value: Status.INPROGRESS
  },
  {
    label: 'Done',
    value: Status.DONE
  },
  {
    label: 'Archive',
    value: Status.ARCHIVE
  }
];

/*
const typeOptions = [
  { label: 'Implementation', value: 1 },
  { label: 'Maintenance', value: 2 },
  { label: 'Bug fixing', value: 3 },
  { label: 'Investigation / evaluation', value: 4 },
  { label: 'Support', value: 5 },
  { label: 'Coaching', value: 6 }
];

const businessScopeOptions = [
  { label: 'Task listings & searches', value: 1 },
  { label: 'Task display & editing', value: 2 },
  { label: 'User management', value: 3 },
  { label: 'Configuration', value: 4 },
  { label: 'Reporting', value: 5 }
];

const technicalScopeOptions = [
  { label: 'Backend development', value: 1 },
  { label: 'Frontend development', value: 2 },
  { label: 'Styling / design', value: 3 },
  { label: 'Infrastructure / deployment', value: 4 }
];

const complexityOptions = [1, 2, 3, 4, 5].map(value => ({ label: value.toString(), value }));
*/

interface TaskFormProps {
  editMode?: boolean;
  formValue: TaskInput;
  onChange: (formValue: TaskInput) => void;
  task?: Task;
}

interface TaskFormState {
  loading: boolean;
  users?: User[];
}

export default class TaskForm extends React.Component<TaskFormProps, TaskFormState> {
  public state: TaskFormState = {
    loading: true
  };

  public componentDidMount = async () =>
    this.setState({
      users: await configuredFetch('/users/list'),
      loading: false
    });

  private onChange = (key: keyof TaskInput) => (value: any) => {
    const formValue = clone(this.props.formValue!) as any;
    formValue[key] = value;
    this.props.onChange(formValue);
  };

  public render = () =>
    this.state.loading ? (
      <LoadingIndicator />
    ) : (
      this.state.users && (
        <div className="flex-container negative-margin-container">
          <div className="flex-item">
            <div className="form-row">
              <label>Title</label>
              <div className="input-container">
                {this.props.editMode ? (
                  <input value={this.props.formValue.title} onChange={onChangeInput(this.onChange('title'))} />
                ) : (
                  <div className="static-input">{this.props.formValue.title}</div>
                )}
              </div>
            </div>
            <div className="form-row">
              <label>Status</label>
              <div className="input-container">
                {this.props.editMode ? (
                  <select value={this.props.formValue.status} onChange={onChangeInput(this.onChange('status'))}>
                    {statusOptions.map(eachStatus => (
                      <option value={eachStatus.value} key={eachStatus.value}>
                        {eachStatus.label}
                      </option>
                    ))}
                  </select>
                ) : (
                  <div className="static-input">
                    {statusOptions.find(({ value }) => value === this.props.formValue.status)!.label}
                  </div>
                )}
              </div>
            </div>
            <div className="form-row">
              <label>Assignee</label>
              <div className="input-container">
                {this.props.editMode ? (
                  <select
                    value={this.props.formValue.assigneeId}
                    onChange={onChangeInput(this.onChange('assigneeId'), true)}
                  >
                    {this.state.users.map(user => (
                      <option value={user._id} key={user._id}>
                        {user.firstName + ' ' + user.lastName}
                      </option>
                    ))}
                  </select>
                ) : (
                  <div className="static-input">
                    {this.state.users.find(({ _id }) => _id === this.props.formValue.assigneeId)!.firstName +
                      ' ' +
                      this.state.users.find(({ _id }) => _id === this.props.formValue.assigneeId)!.lastName}
                  </div>
                )}
              </div>
            </div>
            {this.props.task && (
              <div className="form-row">
                <label>Created by</label>
                <div className="input-container">
                  <div className="static-input">
                    {this.props.task.createdBy.firstName + ' ' + this.props.task.createdBy.lastName}
                  </div>
                </div>
              </div>
            )}
            {this.props.task && (
              <div className="form-row">
                <label>Created at</label>
                <div className="input-container">
                  <div className="static-input">
                    {moment(this.props.task.createdAt).format('MMMM D, YYYY')} (
                    {moment(this.props.task.createdAt).fromNow()})
                  </div>
                </div>
              </div>
            )}
          </div>
          <div className="flex-item">
            <div className="form-row">
              <label>Description</label>
              <div className="input-container">
                {this.props.editMode ? (
                  <textarea
                    value={this.props.formValue.description}
                    onChange={onChangeInput(this.onChange('description'))}
                  />
                ) : (
                  <div className="static-textarea">{this.props.formValue.description}</div>
                )}
              </div>
            </div>
          </div>
        </div>
      )
    );
}
