import * as React from 'react';
import { Route, Switch, RouteComponentProps } from 'react-router';
import Header from '../../components/header';
import EditTaskPage from './edit';
import NewTaskPage from './new';

export default class TaskPage extends React.Component {
  public render = () => (
    <div className="container">
      <Header />
      <Switch>
        <Route path="/task/new" component={NewTaskPage} />
        <Route
          path="/task/:id/edit"
          component={(props: RouteComponentProps<{ id: string }>) => <EditTaskPage {...props} editMode />}
        />
        <Route path="/task/:id" component={EditTaskPage} />
      </Switch>
    </div>
  );
}
