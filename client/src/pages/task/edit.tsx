import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { configuredFetch } from '../../services/auth';
import { Task, TaskInput } from '../../types';
import { clone } from '../../util';
import TaskForm from './task-form';
import LoadingIndicator from '../../components/loading-indicator';
import { Link } from 'react-router-dom';

interface EditTaskPageProps {
  editMode?: boolean;
}

interface EditTaskPageState {
  task?: Task;
  formValue?: TaskInput;
  loading: boolean;
}

export default class EditTaskPage extends React.Component<
  EditTaskPageProps & RouteComponentProps<{ id: string }>,
  EditTaskPageState
> {
  public state: EditTaskPageState = {
    loading: true
  };

  public componentDidMount = async () => {
    const task = await configuredFetch('/tasks/get/' + this.props.match.params.id);
    this.setState({
      task,
      formValue: this.getFormValue(task),
      loading: false
    });
  };

  private getFormValue = (task: Task) => {
    const formValue: any = clone(task);
    formValue.assigneeId = task.assignee._id;
    formValue.assignee = undefined;
    formValue.createdById = task.createdBy._id;
    formValue.createdBy = undefined;
    return formValue as TaskInput;
  };

  private onChange = (formValue: TaskInput) => {
    this.setState({ formValue });
    configuredFetch('/tasks/update/' + this.props.match.params.id, {
      method: 'POST',
      body: JSON.stringify(formValue)
    });
  };

  public render = () =>
    this.state.loading ? (
      <LoadingIndicator />
    ) : (
      this.state.task &&
      this.state.formValue && (
        <div className="content-container">
          <div className="heading">Task #{this.state.task._id}</div>
          <TaskForm
            editMode={this.props.editMode}
            formValue={this.state.formValue}
            onChange={this.onChange}
            task={this.state.task}
          />
          <div className="flex-container">
            <div className="button-group">
              {this.props.editMode ? (
                <Link to={'/task/' + this.state.task._id}>
                  <button>Done</button>
                </Link>
              ) : (
                <Link to={'/task/' + this.state.task._id + '/edit'}>
                  <button>Edit</button>
                </Link>
              )}
            </div>
          </div>
        </div>
      )
    );
}
