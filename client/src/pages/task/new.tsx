import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { configuredFetch, getUser } from '../../services/auth';
import { Status, Task, TaskInput } from '../../types';
import TaskForm from './task-form';

interface NewTaskPageState {
  formValue: TaskInput;
}

export default class NewTaskPage extends React.Component<RouteComponentProps, NewTaskPageState> {
  public state: NewTaskPageState = {
    formValue: {
      title: '',
      status: Status.BACKLOG,
      assigneeId: getUser()!._id,
      description: ''
    }
  };

  private onChange = (formValue: TaskInput) => this.setState({ formValue });

  private onClick = () =>
    configuredFetch('/tasks/create', {
      method: 'POST',
      body: JSON.stringify(this.state.formValue)
    }).then((task: Task) => this.props.history.push('/task/' + task._id));

  public render = () => (
    <div className="content-container">
      <div className="heading">New task</div>
      <TaskForm formValue={this.state.formValue} onChange={this.onChange} editMode />
      <div className="flex-container">
        <div className="button-group">
          <button onClick={this.onClick}>Create</button>
        </div>
      </div>
    </div>
  );
}
