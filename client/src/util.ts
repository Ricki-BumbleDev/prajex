export const padNumber = (
  number: number,
  width: number,
  character?: string
) => {
  const z = character || '0';
  const n = String(number);
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
};

export const shortenString = (string: string, numberOfLetters: number) =>
  string.length > numberOfLetters
    ? string.substring(0, numberOfLetters - 3).trim() + '...'
    : string;

export const onChangeInput = (
  onChange: (value: any) => void,
  number?: boolean
) => (
  event: React.ChangeEvent<
    HTMLInputElement & HTMLTextAreaElement & HTMLSelectElement
  >
) =>
  onChange(
    event.target.type === 'checkbox'
      ? event.target.checked
      : number
      ? Number(event.target.value)
      : event.target.value
  );

export const clone = <T>(obj: T): T => JSON.parse(JSON.stringify(obj));
