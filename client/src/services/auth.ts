import decode from 'jwt-decode';
import { User } from '../types';

interface Token {
  user: User;
  iat: number;
  exp: number;
}

export const login = (email: string, password: string) =>
  configuredFetch('/users/login', {
    method: 'POST',
    body: JSON.stringify({
      email,
      password
    })
  }).then(res => {
    setToken(res.token);
    return res;
  });

export const isLoggedIn = () => {
  const token = getToken();
  return !!token && !isTokenExpired(token);
};

export const isTokenExpired = (token: string) => {
  try {
    const decoded = decode<Token>(token);
    return decoded.exp < Date.now() / 1000;
  } catch (err) {
    return false;
  }
};

const setToken = (token: string) => localStorage.setItem('token', token);

const getToken = () => localStorage.getItem('token');

export const logout = () => localStorage.removeItem('token');

export const getUser = () => {
  const token = getToken();
  return token ? decode<Token>(token).user : null;
};

export const configuredFetch = (url: string, options?: RequestInit) => {
  const headers: Record<string, any> = {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  };
  if (isLoggedIn()) {
    headers['Authorization'] = 'Bearer ' + getToken();
  }

  return fetch(url, {
    headers,
    ...options
  })
    .then(checkStatus)
    .then(response => response.json());
};

const checkStatus = async (response: Response) => {
  if (response.status >= 200 && response.status < 300) {
    return response;
  } else {
    const error = await response.json().catch(() => ({ message: response.statusText }));
    throw new Error(error.message);
  }
};
