export interface User {
  _id: number;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
}

export interface Task {
  _id: number;
  title: string;
  status: Status;
  description: string;
  assignee: User;
  createdAt: Date;
  createdBy: User;
}

export interface TaskInput {
  title: string;
  status: Status;
  description: string;
  assigneeId: number;
}

export enum Status {
  BACKLOG = 'BACKLOG',
  TODO = 'TODO',
  INPROGRESS = 'INPROGRESS',
  DONE = 'DONE',
  ARCHIVE = 'ARCHIVE'
}
