import bcrypt from 'bcrypt';
import fs from 'fs';
import tunnel from 'tunnel-ssh';
import getDb, { connectToDb, getAutoIncrementId } from '../src/db';
import { MongoClient } from 'mongodb';

const dbUrl = process.env.DB_URL || 'mongodb://localhost:27017';
const dbName = process.env.DB_NAME || 'prajex';

const userNames = [
  {
    firstName: 'Frederik',
    lastName: 'Hummel'
  },
  {
    firstName: 'Steven',
    lastName: 'Spencer'
  },
  {
    firstName: 'Walter',
    lastName: 'Anderson'
  },
  {
    firstName: 'Clare',
    lastName: 'Montgomery'
  },
  {
    firstName: 'Giacomo',
    lastName: 'Mariano'
  },
  {
    firstName: 'Robin',
    lastName: 'Owen'
  },
  {
    firstName: 'Michael',
    lastName: 'Carpenter'
  },
  {
    firstName: 'Clark',
    lastName: 'Porter'
  },
  {
    firstName: 'Lynn',
    lastName: 'Brooks'
  },
  {
    firstName: 'Walton',
    lastName: 'Cohen'
  },
  {
    firstName: 'Jessica',
    lastName: 'McLaughlin'
  }
];

const users = userNames.map((userName, index) => ({
  ...userName,
  email: `${userName.firstName.toLowerCase()}.${userName.lastName.toLowerCase()}@bumbledev.com`,
  password: `praj${index}x`
}));

const executeSequential = (promises: Promise<any>[]) =>
  promises.reduce((promiseChain, currentTask) => {
    return promiseChain.then(chainResults => currentTask.then(currentResult => [...chainResults, currentResult]));
  }, Promise.resolve([]));

const main = async () => {
  const sshTunnelConfig = {
    agent: process.env.SSH_AUTH_SOCK,
    username: 'root',
    privateKey: fs.readFileSync('/Users/fhummel/.ssh/id_rsa'),
    host: '94.16.123.161',
    port: 22,
    dstHost: '127.0.0.1',
    dstPort: 27017,
    localHost: '127.0.0.1',
    localPort: 27017
  };

  let db;

  await new Promise((resolve, reject) =>
    tunnel(sshTunnelConfig, async (error, server) => {
      if (error) {
        reject(error);
      }
      db = await MongoClient.connect(dbUrl).then(client => client.db(dbName));
      resolve();
    })
  );

  await executeSequential(
    users.map(async user => {
      db.collection('users').insertOne({
        _id: await getAutoIncrementId('users'),
        ...user,
        password: await bcrypt.hash(user.password, 10)
      });
    })
  );
};

main();
