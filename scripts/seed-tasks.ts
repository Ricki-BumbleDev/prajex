import getDb, { connectToDb, getAutoIncrementId } from '../src/db';

const dbUrl = process.env.DB_URL || 'mongodb://localhost:27017';
const dbName = process.env.DB_NAME || 'prajex';

const tasks = [
  {
    title: 'Security',
    status: 'DONE',
    description: 'Implement JWT security including login and logout',
    createdAt: '2018-12-28T17:44:19.053Z',
    assigneeId: 1,
    createdById: 1
  },
  {
    title: 'Task creation',
    status: 'DONE',
    description: 'Users should be able to create new tasks',
    createdAt: '2018-12-28T17:49:54.380Z',
    assigneeId: 1,
    createdById: 1
  },
  {
    title: 'Search',
    status: 'DONE',
    description: 'Users should be able to search for tasks by ID, name and/or description',
    createdAt: '2018-12-28T17:50:41.633Z',
    assigneeId: 1,
    createdById: 1
  },
  {
    title: 'User registration',
    status: 'BACKLOG',
    description: 'Users should be able to create an user account for themselves',
    createdAt: '2018-12-28T17:51:36.888Z',
    assigneeId: 1,
    createdById: 1
  },
  {
    title: 'Investigation: Multiple projects',
    status: 'BACKLOG',
    description: 'Users should be able to manage multiple projects',
    createdAt: '2018-12-28T17:52:30.324Z',
    assigneeId: 1,
    createdById: 1
  },
  {
    title: 'Investigation: General user management',
    status: 'BACKLOG',
    description: 'Project permissions, groups / teams',
    createdAt: '2018-12-28T17:54:07.580Z',
    assigneeId: 1,
    createdById: 1
  },
  {
    title: 'Status transition shortcuts',
    status: 'TODO',
    description: 'Task cards should include shortcuts to transition to the respectively following status',
    createdAt: '2018-12-28T17:56:01.981Z',
    assigneeId: 1,
    createdById: 1
  },
  {
    title: 'Responsive design',
    status: 'TODO',
    description: 'Optimize for display on different screen sizes, particularly mobile',
    createdAt: '2018-12-28T18:46:42.738Z',
    assigneeId: 1,
    createdById: 1
  },
  {
    title: 'Password encryption',
    status: 'DONE',
    description: 'Use "bcrypt" for password encryption',
    createdAt: '2018-12-28T21:36:48.281Z',
    assigneeId: 1,
    createdById: 1
  },
  {
    title: 'Drag and drop',
    status: 'DONE',
    description: 'Enable status transitions by dragging and dropping task cards across the status columns of the board',
    createdAt: '2018-12-30T21:24:58.794Z',
    assigneeId: 1,
    createdById: 1
  },
  {
    title: 'React Select integration',
    status: 'BACKLOG',
    description: 'Use react-select for select inputs / dropdowns',
    assigneeId: 1,
    createdAt: new Date(1546630333404),
    createdById: 1
  },
  {
    title: 'Task types',
    status: 'BACKLOG',
    description:
      'A type should be assignable to each task. Ideas for types are "Implementation", "Investigation", "Maintenance", "Bug fix" and "Support".',
    createdAt: '2019-01-04T19:35:47.884Z',
    assigneeId: 1,
    createdById: 1
  },
  {
    title: 'Task scopes',
    status: 'BACKLOG',
    description:
      'A business and a technical scope should be assignable to each task. The business scope describes the affiliation of a task to the business problem or aspect it aims to solve. The technical scope defines which technical parts of the project is mainly effected by the task (e.g. frontend / backend).',
    assigneeId: 1,
    createdAt: new Date(1547719145851),
    createdById: 1
  },
  {
    title: 'Investigation: Portraing "blocks / is blocked by" relationships',
    status: 'BACKLOG',
    description:
      'Investigate how "blocks / is blocked by" relationships can be portrayed and edited in an unobtrusive yet comprehensible way.',
    assigneeId: 1,
    createdAt: new Date(1547719462056),
    createdById: 1
  },
  {
    title: 'Investigation: Task packages',
    status: 'BACKLOG',
    description:
      'Tasks can be summarized in task packages. Task packages can then be numbered to define the order / priority in which the tasks should be executed.',
    assigneeId: 1,
    createdAt: new Date(1547719758373),
    createdById: 1
  },
  {
    title: 'Effort estimation',
    status: 'BACKLOG',
    description:
      'The effort of each task should be definable in a respective field using complexity points ranging from 1 to 6.',
    assigneeId: 1,
    createdAt: new Date(1547719958065),
    createdById: 1
  }
];

const executeSequential = (promises: Promise<any>[]) =>
  promises.reduce((promiseChain, currentTask) => {
    return promiseChain.then(chainResults => currentTask.then(currentResult => [...chainResults, currentResult]));
  }, Promise.resolve([]));

const main = async () => {
  await connectToDb(dbUrl, dbName);

  await executeSequential(
    tasks.map(async task => {
      getDb()
        .collection('tasks')
        .insertOne({
          _id: await getAutoIncrementId('tasks'),
          ...task
        });
    })
  );
};

main();
