FROM node:12-alpine as builder
RUN apk add python make g++
COPY . /app
WORKDIR /app
RUN npm ci
RUN cd client && npm ci
RUN npm run build

FROM node:12-alpine
RUN apk add python make g++
RUN mkdir /app
WORKDIR /app
COPY --from=builder /app/build build
COPY --from=builder /app/package.json /app/package-lock.json ./
RUN npm ci --only=prod
CMD npm start
